function plot_spectrum(signal, fs, M)

f = linspace(-fs/2, fs/2, M);
N = length(signal);

for k = 1:M
    temp = 0;
    
for i = 1:N
    temp = temp + signal(i) * exp(-j*2*pi*f(k)*(i-1)*(1/fs));
end 


spec(k) = temp;

end

plot(f, abs(spec));

end

