close all; clc; clear all;

% Load data
%data = load('ult_sig.mat');


% Plot raw data
%plot(data.ult_sig);

%%

% M is length of Discrete Fourier Transform, fs is the sampling frequence
% and a is the amplitude
M = 101;
fs = 100;
a = 3;

% Generate rectangular signal
signal = a*ones(1, 20);

% Plot Amplitude Spectrum and generate Fourier Spectrum Values
spec = plot_spectrum(signal, fs, M);
f = linspace(-fs/2, fs/2, M);

%f_spec = [spec;f];

left = (fs/2)-1;
spec(1:left) = 0;

right =  (fs/2)+1;
spec(right:length(spec)) = 2*spec(right:length(spec));

ht = spec;

plot(f, abs(ht));

title('HT Spectrum');
xlabel('Frequency [Hz]');
ylabel('Amplitude [Volt]');
legend('Analog Signal');


%%


%for i = 1:f
%    for k = 1:spec
%        if f(k) <= 0
%            spec(i) = 0;
%        elseif f(k) == 0
%            spec(i) = spec;
%        elseif f(k) >= 0
%            spec(i) = 2*spec;
%        end    

%    end
%end





%%

%for i = 1:size(spec, 1)
    %ht = 0;
%    hil = ht(i);

%for k = 1:size(f, 1)
%    if f(k) <= 0
%        ht = ht + 0;
%    elseif f(k) == 0
%        ht = ht + spec(k);
%    elseif f(k) >= 0
%        ht = ht + 2*spec(k);
%    end
%end
    
%hil(i) = ht;
    
%end
    
%plot(f, abs(hil));

%title('HT Spectrum');
%xlabel('Frequency [Hz]');
%ylabel('Amplitude [Volt]');
%legend('Digital Signal');
%end









%%




% Hilbert Transformation
%for n = 1:fs/fs
%    zx = temp + j*conv(temp, 1/pi*n);
    
%    if temp > 0
%        zx = 2*zx;
%    elseif temp == 0
%        zx = zx;
%    elseif temp < 0
%        zx = 0;
%    end
    




%for tau = 1:M
%    tau = 0;

%for n = 1:fs/fs
    %analog = signal(n) + j*signal(n) * 1/pi*temp;
    %analog = exp(j*2*pi*(1/fs)/2) .* temp .* sin(pi*fs)/(pi*fs));
    
    % Fourier Transformation
    %analog = exp(j*2*pi*(1/fs)/2) .* signal(tau) .* sin(pi*fs)/(pi*fs);
    %analog = signal(tau) .* sin((pi*fs*tau)/(pi*fs*tau));
%end
%end


%plot(f, abs(spec));
%hold on 
%spect(k) = zx;
%plot(f, spect);